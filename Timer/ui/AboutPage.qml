import QtQuick 2.4
import Ubuntu.Components 1.3

Page {
    id: root_about

    header: PageHeader {
        id: main_header

        SwipeToAction {
          apl_page: root_about;
          swipeDistance: swipe_Distance;
          isOneCol: true;
        }

        title: i18n.tr("About")
        StyleHints {backgroundColor: top_back_color; foregroundColor: top_text_color}
        leadingActionBar.actions: [
            Action {
                visible: isWide
                text: "close"
                iconName: "close"
                onTriggered: {
                    isOneColumnLayout = true
                    apl_main.removePages(root_about)
                }
            },
            Action {
                id: collapse_favs
                visible: !isWide
                text: "back"
                iconSource: Qt.resolvedUrl(backIcon)
                onTriggered: {
                    isOneColumnLayout = true
                    apl_main.removePages(root_about)
                }
            }
        ]
    }

    Sections {
        id: header_sections
        StyleHints {selectedSectionColor: top_text_color; }
        anchors {
            top: main_header.bottom
            topMargin: units.gu(1)
            horizontalCenter: parent.horizontalCenter
        }
        model: [i18n.tr("General"), i18n.tr("Credits"), i18n.tr("Important")]
    }

    Flickable {
        id: page_flickable

        flickableDirection: Flickable.AutoFlickIfNeeded

        anchors {
            top: header_sections.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        contentHeight:  main_column.height + units.gu(2)
        clip: true

        Column {
            id: main_column

            anchors {
                left: parent.left
                right: parent.right
                top: parent.top
                leftMargin: units.gu(1)
                rightMargin: units.gu(1)
            }

            Item {
                id: icon

                visible: header_sections.selectedIndex === 0
                width: parent.width
                height: app_icon.height + units.gu(4)

                UbuntuShape {
                    id: app_icon

                    width: Math.min(root_about.width/3, 256)
                    height: width
                    anchors.centerIn: parent

                    source: Image {
                        id: icon_image
                        source: Qt.resolvedUrl("Timer.png")
                    }
                    radius: "small"
                    aspect: UbuntuShape.DropShadow
                }
            }

            Label {
                id: name

                visible: header_sections.selectedIndex === 0
                text: i18n.tr("Timer") + " v%1".arg(Qt.application.version)
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottomMargin: units.gu(4)
                textSize: Label.Large
                horizontalAlignment:  Text.AlignHCenter
            }

            ListItem {
                visible: header_sections.selectedIndex === 0
                height: l_bugs.height + divider.height
                ListItemLayout {
                    id: l_bugs
                    title.text: i18n.tr("Report bugs on GitLab")
                    ProgressionSlot{name: "external-link";color:top_text_color}
                }
                onClicked: Qt.openUrlExternally('https://gitlab.com/Danfro/timer/issues')
            }

            ListItem {
                visible: header_sections.selectedIndex === 0
                height: l_trans.height + divider.height
                ListItemLayout {
                    id: l_trans
                    // title.text: i18n.tr("Help with translation via translate-ut.org")
                    title.text: i18n.tr("Help with translation via GitLab")
                    ProgressionSlot{name: "external-link";color:top_text_color}
                }
                // onClicked: Qt.openUrlExternally('https://translate-ut.org/projects/timer/timer-app/')
                onClicked: Qt.openUrlExternally('https://gitlab.com/Danfro/timer/tree/master/po')
            }

            ListItem {
                visible: header_sections.selectedIndex === 0
                height: l_apps.height + divider.height
                ListItemLayout {
                    id: l_apps
                    title.text: i18n.tr("Timer app in OpenStore")
                    ProgressionSlot{name: "external-link";color:top_text_color}
                }
                onClicked: Qt.openUrlExternally('https://open-store.io/app/timerpro.mivoligo')
            }

            ListItem {
                visible: header_sections.selectedIndex === 0
                height: l_license.height + divider.height
                ListItemLayout {
                    id: l_license
                    title.text: i18n.tr("License") + ": BSD-3-Clause"
                    ProgressionSlot{name: "external-link";color:top_text_color}
                }
                onClicked: {Qt.openUrlExternally('https://opensource.org/licenses/BSD-3-Clause')}
            }

            ListItem {
                visible: header_sections.selectedIndex === 0

                ListItemLayout {
                    title.text: i18n.tr("Donate to UBports")
                    subtitle.text: i18n.tr("UBports is providing Ubuntu Touch.")
                    ProgressionSlot {name: "external-link"; color:top_text_color; }
                }

                //onClicked: Qt.openUrlExternally('https://www.paypal.me/miv')
                //Donate link changed to UBports donate page
                onClicked: Qt.openUrlExternally('https://ubports.com/en_EN/donate')
            }

            ListItem {
                visible: header_sections.selectedIndex === 0

                ListItemLayout {
                    title.text: i18n.tr("Donate to Michał Prędotka")
                    subtitle.text: i18n.tr("Michał Prędotka developed this great app.")
                    ProgressionSlot {name: "external-link"; color:top_text_color; }
                }

                onClicked: Qt.openUrlExternally('https://www.paypal.me/miv')
            }

            ListItem {
                visible: header_sections.selectedIndex === 0
                divider.visible: false

                ListItemLayout {
                    title.text: i18n.tr("Donate to me")
                    subtitle.text: i18n.tr("I am maintaining and improving this app.")
                    ProgressionSlot {name: "external-link"; color:top_text_color; }
                }

                onClicked: Qt.openUrlExternally('https://paypal.me/payDanfro')
            }

            Label {
                id: actualdev

                visible: header_sections.selectedIndex === 1
                text: "\n" + i18n.tr("App development since version v1.2")
                anchors.horizontalCenter: parent.horizontalCenter
                font.bold : true
                horizontalAlignment:  Text.AlignHCenter
            }

            ListItem {
                visible: header_sections.selectedIndex === 1
                height: l_maintainer.height + divider.height
                ListItemLayout {
                    id: l_maintainer
                    title.text: i18n.tr("Maintainer") + ": Daniel Frost"
                    ProgressionSlot {name: "external-link"; color:top_text_color; }
                }
                onClicked: {Qt.openUrlExternally('https://gitlab.com/Danfro')}
            }

            ListItem {
                visible: header_sections.selectedIndex === 1
                height: l_french.height - units.gu(1)
                divider.visible: false
                ListItemLayout {
                    id: l_french
                    title.text: i18n.tr("French") + ": Anne Onyme 017, AppsLee"
                    ProgressionSlot {name: "external-link"; color:top_text_color; }
                }
                onClicked: {Qt.openUrlExternally('https://gitlab.com/Anne17')}
            }

            ListItem {
                visible: header_sections.selectedIndex === 1
                height: l_spanish.height - units.gu(1)
                divider.visible: false
                ListItemLayout {
                    id: l_spanish
                    title.text: i18n.tr("Spanish") + ": Krakakanok, advocatux"
                    ProgressionSlot {name: "external-link"; color:top_text_color; }
                }
                onClicked: {Qt.openUrlExternally('https://gitlab.com/Krakakanok')}
            }

            Label {
                id: historicaldev

                visible: header_sections.selectedIndex === 1
                text: "\n" + i18n.tr("App development up to version v1.1")
                anchors.horizontalCenter: parent.horizontalCenter
                font.bold : true
                horizontalAlignment:  Text.AlignHCenter
            }

            ListItem {
                visible: header_sections.selectedIndex === 1
                height: l_author.height
                ListItemLayout {
                    id: l_author
                    title.text: i18n.tr("Author") + ": Michał Prędotka"
                    ProgressionSlot {name: "external-link"; color:top_text_color; }
                }
                onClicked: {Qt.openUrlExternally('http://mivoligo.com')}
            }

            ListItem {
                visible: header_sections.selectedIndex === 1
                height: l_sound.height + divider.height
                ListItemLayout {
                    id: l_sound
                    title.text: i18n.tr("Sounds") + ": Tyrel Parker"
                    ProgressionSlot {name: "external-link"; color:top_text_color; }
                }
                onClicked: {Qt.openUrlExternally('https://plus.google.com/u/0/108131512413210328857/about')}
            }

            ListItem {
                visible: header_sections.selectedIndex === 1
                height: l_icon.height + divider.height
                ListItemLayout {
                    id: l_icon
                    title.text: i18n.tr("Icon design") + ": Sam Hewitt"
                    ProgressionSlot {name: "external-link"; color:top_text_color; }
                }
                onClicked: {Qt.openUrlExternally('https://samuelhewitt.com﻿')}
            }

            ListItem {
                visible: header_sections.selectedIndex === 1
                height: l_test.height + divider.height
                ListItemLayout {
                    id: l_test
                    title.text: i18n.tr("Testing") + ": Sergi Quiles Pérez"
                    ProgressionSlot {name: "external-link"; color:top_text_color; }
                }
                onClicked: {Qt.openUrlExternally('https://plus.google.com/u/0/+SergiQuilesPérez')}
            }

            ListItem {
                visible: header_sections.selectedIndex === 1
                divider.visible: false
                ListItemLayout {
                    title.text: i18n.tr("Special thanks") + ": nik90"
                    //ProgressionSlot {name: "external-link"; color:top_text_color; }
                }
                //Link does not work anymore -> disabled
                //onClicked: {Qt.openUrlExternally('http://nik90.com')}
            }

            //Warning Label, changes need to go to: /Timer/ui/Warning.qml, README.md and OpenStore description text
            Label {
                id: warning

                visible: header_sections.selectedIndex === 2

                horizontalAlignment: Text.AlignJustify //Text.AlignHCenter
                width: parent.width //- units.gu(2)
                text: "\n"
                    + i18n.tr("Please notice that the app depends on the Alarm API which has some limitations. There are a few things you need to know:")
                    + "\n\n"
                    + "1. " + i18n.tr("Setting timers shorter than 1 minute is not recommended. Due to system limitations, the notifications about such timers are not displayed at the right time. Sorry.")
                    + "\n\n"
                    + "2. " + i18n.tr("Sound volume is the same as set in the Clock app for alarms.")
                    + "\n\n"
                    + "3. " + i18n.tr("Changing time settings or time zone when timer is running will confuse the timer.")
                    + "\n\n"
                    + "4. " + i18n.tr("Timers are registered as alarms within the clock app. Once the timer has elapsed it is removed from clock app.")
                    + "\n\n"
                    + "5. " + i18n.tr("You can set up to four timers.")
                    + "\n\n"
                    + i18n.tr("Feel free to report other problems:") + "\n"
                wrapMode: Text.WordWrap
            }
            ListItem {
                visible: header_sections.selectedIndex === 2
                divider.visible: false
                height: bugreport.height

                Label {
                    id: bugreport
                    text: i18n.tr("Report an issue on GitLab")
                    color: theme.palette.normal.activity
                    horizontalAlignment: Text.AlignHLeft

                    MouseArea {
                        anchors.fill: parent
                        onClicked: Qt.openUrlExternally('https://gitlab.com/Danfro/timer/issues')
                    }
                }
            }
        }
    }
}
