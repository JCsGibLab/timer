# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-03-19 18:27+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/AboutPage.qml:16
#: /media/ubuntu/Daten/Forks/timer/Timer/ui/MainPage.qml:92
msgid "About"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/AboutPage.qml:49
msgid "General"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/AboutPage.qml:49
msgid "Credits"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/AboutPage.qml:49
msgid "Important"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/AboutPage.qml:104
#: /media/ubuntu/Daten/Forks/timer/Timer/ui/MainPage.qml:32
#: /media/ubuntu/Daten/Forks/timer/Timer/ui/TimerElement.qml:106
#: /media/ubuntu/Daten/Forks/timer/Timer/ui/TimerSetterExtra.qml:28
#: /media/ubuntu/Daten/Forks/timer/Timer/ui/TimerSetterExtra.qml:299
#: /media/ubuntu/Daten/Forks/timer/Timer/ui/TimerSetterMain.qml:14
#: /media/ubuntu/Daten/Forks/timer/Timer/ui/TimerSetterMain.qml:33
#: /media/ubuntu/Daten/Forks/timer/build/all/app/Timer/Timer.desktop.h:1
msgid "Timer"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/AboutPage.qml:116
msgid "Report bugs on GitLab"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/AboutPage.qml:128
msgid "Help with translation via GitLab"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/AboutPage.qml:140
msgid "Timer app in OpenStore"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/AboutPage.qml:151
msgid "License"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/AboutPage.qml:161
msgid "Donate to UBports"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/AboutPage.qml:162
msgid "UBports is providing Ubuntu Touch."
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/AboutPage.qml:175
msgid "Donate to Michał Prędotka"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/AboutPage.qml:176
msgid "Michał Prędotka developed this great app."
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/AboutPage.qml:188
msgid "Donate to me"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/AboutPage.qml:189
msgid "I am maintaining and improving this app."
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/AboutPage.qml:200
msgid "App development since version v1.2"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/AboutPage.qml:211
msgid "Maintainer"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/AboutPage.qml:223
msgid "French"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/AboutPage.qml:235
msgid "Spanish"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/AboutPage.qml:245
msgid "App development up to version v1.1"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/AboutPage.qml:256
msgid "Author"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/AboutPage.qml:267
msgid "Sounds"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/AboutPage.qml:278
msgid "Icon design"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/AboutPage.qml:289
msgid "Testing"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/AboutPage.qml:299
msgid "Special thanks"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/AboutPage.qml:315
msgid ""
"Please notice that the app depends on the Alarm API which has some "
"limitations. There are a few things you need to know:"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/AboutPage.qml:317
#: /media/ubuntu/Daten/Forks/timer/Timer/ui/Warning.qml:40
msgid ""
"Setting timers shorter than 1 minute is not recommended. Due to system "
"limitations, the notifications about such timers are not displayed at the "
"right time. Sorry."
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/AboutPage.qml:319
#: /media/ubuntu/Daten/Forks/timer/Timer/ui/Warning.qml:42
msgid "Sound volume is the same as set in the Clock app for alarms."
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/AboutPage.qml:321
#: /media/ubuntu/Daten/Forks/timer/Timer/ui/Warning.qml:44
msgid ""
"Changing time settings or time zone when timer is running will confuse the "
"timer."
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/AboutPage.qml:323
#: /media/ubuntu/Daten/Forks/timer/Timer/ui/Warning.qml:46
msgid ""
"Timers are registered as alarms within the clock app. Once the timer has "
"elapsed it is removed from clock app."
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/AboutPage.qml:325
#: /media/ubuntu/Daten/Forks/timer/Timer/ui/Warning.qml:48
msgid "You can set up to four timers."
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/AboutPage.qml:327
msgid "Feel free to report other problems:"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/AboutPage.qml:337
msgid "Report an issue on GitLab"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/AppThemePage.qml:17
#: /media/ubuntu/Daten/Forks/timer/Timer/ui/SettingsPage.qml:118
msgid "App theme"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/ClockThemePage.qml:17
#: /media/ubuntu/Daten/Forks/timer/Timer/ui/SettingsPage.qml:104
msgid "Clock theme"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/DefaultSoundSettingsPage.qml:24
#: /media/ubuntu/Daten/Forks/timer/Timer/ui/SettingsPage.qml:88
msgid "Default sound"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/DefaultSoundSettingsPage.qml:24
msgid "Timer sound"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/DefaultSoundSettingsPage.qml:68
#: /media/ubuntu/Daten/Forks/timer/Timer/ui/SoundPickerPage.qml:39
msgid "Stop playing"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/DefaultSoundSettingsPage.qml:68
#: /media/ubuntu/Daten/Forks/timer/Timer/ui/SoundPickerPage.qml:39
msgid "Play selected"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/EditTimer.qml:19
msgid "Edit timer"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/EditTimer.qml:19
msgid "Add to favourites"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/EditTimer.qml:37
msgid "Save"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/EditTimer.qml:261
#: /media/ubuntu/Daten/Forks/timer/Timer/ui/TimerPreview.qml:376
#: /media/ubuntu/Daten/Forks/timer/Timer/ui/TimerSetterExtra.qml:300
#: /media/ubuntu/Daten/Forks/timer/Timer/ui/TimerSetterMain.qml:538
msgid "Timer name"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/FavPage.qml:53
#: /media/ubuntu/Daten/Forks/timer/Timer/ui/MainPage.qml:58
msgid "Favourites"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/FavPage.qml:59
msgid "Add new"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/FavPage.qml:74
msgid "Delete"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/FavPage.qml:84
msgid "Select All"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/FavPage.qml:156
msgid "You can add timers as favourites by pressing the + icon above."
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/InfoBubble.qml:37
msgid "You can add a new timer by pressing the + icon above."
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/MainPage.qml:24
msgid "Quit"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/MainPage.qml:31
msgid "Timers"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/MainPage.qml:41
#: /media/ubuntu/Daten/Forks/timer/Timer/ui/MainPage.qml:109
msgid "Add new timer"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/MainPage.qml:75
#: /media/ubuntu/Daten/Forks/timer/Timer/ui/SettingsPage.qml:16
msgid "Settings"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/MainPage.qml:128
msgid "Timer details"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/SettingsPage.qml:70
msgid "Keep display on"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/SettingsPage.qml:131
msgid "Show close button on main page"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/SettingsPage.qml:132
msgid "Disable to hide close button"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/SettingsPage.qml:132
msgid "Enable to show close button"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/SettingsPage.qml:153
#: /media/ubuntu/Daten/Forks/timer/Timer/ui/SwipeDistancePage.qml:17
msgid "Swipe distance"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/SettingsPage.qml:154
msgid "Percent of screen width in portrait mode"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/SoundPickerPage.qml:68
msgid "Timer sounds"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/SoundPickerPage.qml:134
msgid "System sounds"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/TimerElement.qml:52
#: /media/ubuntu/Daten/Forks/timer/Timer/ui/TimerElement.qml:78
#: /media/ubuntu/Daten/Forks/timer/Timer/ui/TimerPreview.qml:92
#: /media/ubuntu/Daten/Forks/timer/Timer/ui/TimerSetterExtra.qml:67
#: /media/ubuntu/Daten/Forks/timer/Timer/ui/TimerSetterMain.qml:143
msgid "tomorrow"
msgstr ""

#. TRANSLATORS: It's about the time at which a timer will finish,
#. It will read for example "Finish 12:45"
#: /media/ubuntu/Daten/Forks/timer/Timer/ui/TimerElement.qml:158
#: /media/ubuntu/Daten/Forks/timer/Timer/ui/TimerElement.qml:167
#: /media/ubuntu/Daten/Forks/timer/Timer/ui/TimerPreview.qml:116
#: /media/ubuntu/Daten/Forks/timer/Timer/ui/TimerSetterExtra.qml:79
#: /media/ubuntu/Daten/Forks/timer/Timer/ui/TimerSetterMain.qml:166
msgid "Finish"
msgstr ""

#. TRANSLATORS: This string is displayed after a timer has finished.
#. "%1" is replaced by time and the string reads for example:
#. "Timer PIZZA finished 00:02:35 ago".
#. Timer name is not marked for translation
#: /media/ubuntu/Daten/Forks/timer/Timer/ui/TimerElement.qml:328
#: /media/ubuntu/Daten/Forks/timer/Timer/ui/TimerPreview.qml:543
#: /media/ubuntu/Daten/Forks/timer/Timer/ui/TimerSetterMain.qml:659
#, qt-format
msgid " finished %1 ago"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/TimerElement.qml:340
#: /media/ubuntu/Daten/Forks/timer/Timer/ui/TimerPreview.qml:555
#: /media/ubuntu/Daten/Forks/timer/Timer/ui/TimerSetterMain.qml:671
msgid "Okay"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/Warning.qml:10
msgid "Welcome!"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/Warning.qml:38
msgid "IMPORTANT!"
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/Warning.qml:50
msgid "Feel free to report other problems."
msgstr ""

#: /media/ubuntu/Daten/Forks/timer/Timer/ui/Warning.qml:64
msgid "OK, I understand"
msgstr ""
